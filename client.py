from socket import *
import sys

host = sys.argv[1] # get host from terminal
port = int(sys.argv[2]) # get port from terminal
s = socket(AF_INET,SOCK_STREAM) # set up socket
s.connect((host,port)) # connect to server
while True:
	reply = ""
	while (reply[-2:] != "\r\n"): # keeps data transfer open untill a '\r\n' is received
		reply += s.recv(1024) # gets data from the server
	print reply # print what the server sent
	message = raw_input("cmd> ") # get input from user
	s.send(message + '\r\n') # send message to sever
	if message == "adios": # if we said goodbye exit
		break
