from socket import *
import time, datetime, sys

chatBuffer = []
clientName = "unknown"
go = True
helpList = ["'help'","'test:' -words","'get'","'push:' -words","'getrange' -firstindex - endindex","'adios'","reverse - words"]

def doCommand(commandStr):
    global clientName, chatBuffer, go
    commands = commandStr.split(':') # spliting on : insures that we split the command from the words
    command = commands[0]
    if command == "name":
        clientName = commands[1][1:] # becuase we split on : theres aleading space on the name this will remove it
        return "OK" + '\r\n'
    elif command == "test":
        return commands[1][1:] + '\r\n' # commands[1] holds all the words sent after test
    elif command == "push":
        newLine = str(clientName) + ": " + commands[1][1:] + "\t" + datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        chatBuffer.append(newLine) # add the new line to the global array
        return "OK" + '\r\n'
    elif command == "get":
        return '\n'.join(chatBuffer) + '\r\n' # join all of the chat lines with '\n'
    elif commandStr.find("getrange") != -1: # becuase there is no : for getrange we need to check this way
        params = commandStr.split() # we need to split to get the number params
        return '\n'.join(chatBuffer[int(params[1]):int(params[2]) + 1]) + '\r\n'
    elif command == "adios":
        clientName = "unknown" # user is leaving set the username to unknown
        go = False # this will allow the loop to exit
        return "CLOSE SOCKET"
    elif command == "help":
        return '\n'.join(helpList) + '\r\n' # join all of the helpList with '\n'
    elif command == "reverse":
        return commands[1][::-1] + '\r\n' # tricky python to reverse a string
    else:
        return "Error: unrecognized command: " + command + '\r\n'

port = int(sys.argv[1]) # gets port from terminal
 # sets socket type, binds it to a port, sets it to listen and accept incomming clients
s = socket(AF_INET,SOCK_STREAM)
s.bind(('',port))
s.listen(1)
connection, address = s.accept()
connection.sendall("Welcome to Niles' chat room\r\n")
print "Connected by", address
while go:
    data = ""
    while (data[-2:] != "\r\n"): # keeps data transfer open untill a '\r\n' is received
        data += connection.recv(1024) # gets data from the client
    for subCmdStr in data.split("\r\n"): # splits and removes '\r\n' in between words
        if subCmdStr != '': # the above split can sometime result in a ''
            connection.sendall(doCommand(subCmdStr)) # process command and send
connection.close() # once the loop is exited the connection will close
